const mongoose = require('mongoose');
const config = require('../config/config.conf');

mongoose
    .connect(config.DB_LINK)
    .then(() => console.log('Successful Connection'))
    .catch((err) => console.log(err));
