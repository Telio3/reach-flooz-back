const express = require('express');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./specs/specs.yaml');
const routes = require('./routes');
require('./database');
const cookieParser = require('cookie-parser');

const app = express();

exports.app = app;

app.use(cookieParser());
require('./config/jwt.config');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(routes);
