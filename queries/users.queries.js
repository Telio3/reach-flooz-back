const User = require('../database/models/user.model');

exports.getUsers = () => {
    return User.find({}).exec();
};

exports.getUserById = (userId) => {
    return User.findOne({ _id: userId }).exec();
};

exports.getUserByMail = (email) => {
    return User.findOne({ 'local.email': email }).exec();
};

exports.createUser = async (user) => {
    try {
        const hashedPassword = await User.hashPassword(user.password);
        const newUser = new User({
            username: user.username,
            chips: 500,
            local: {
                email: user.email,
                password: hashedPassword,
            },
        });
        return newUser.save();
    } catch (e) {
        throw e;
    }
};

exports.updateUserChips = (userId, body) => {
    return User.findByIdAndUpdate(userId, { $set: { chips: body.chips } }, { new: true });
};

exports.topFiveUsers = () => {
    return User.find({}, null, { sort: { chips: -1 }, limit: 5 }).exec();
};
