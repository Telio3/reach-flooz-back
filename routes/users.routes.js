const router = require('express').Router();
const { ensureAuthenticated } = require('../config/security.config');
const { getUsers, getUserById, createUser, updateUserChips } = require('../controllers/users.controller');

router.get('/', getUsers);
router.get('/:idUser', getUserById);
router.post('/', createUser);
router.put('/:idUser', [ensureAuthenticated, updateUserChips]);

module.exports = router;
