const { getUsers, getUserById, createUser, updateUserChips, topFiveUsers } = require('../queries/users.queries');

exports.getUsers = async (req, res, next) => {
    try {
        let users = [];

        if (Number(req.query?.ranked)) {
            users = await topFiveUsers();
        } else {
            users = await getUsers();
        }

        res.json(users);
    } catch (e) {
        next(e);
    }
};

exports.getUserById = async (req, res, next) => {
    try {
        const user = await getUserById(req.params.idUser);
        res.json(user);
    } catch (e) {
        res.json(null);
    }
};

exports.createUser = async (req, res, next) => {
    try {
        const body = req.body;
        const newUser = await createUser(body);
        res.cookie('idUser', newUser._id.toString());
        req.login(newUser);
        res.json(newUser);
    } catch (e) {
        res.status(400).send({ message: 'Email déjà utilisée' });
    }
};

exports.updateUserChips = async (req, res, next) => {
    try {
        const idUser = req.params.idUser;
        const body = req.body;
        const updatedUserChips = await updateUserChips(idUser, body);
        res.json(updatedUserChips);
    } catch (e) {
        next(e);
    }
};
