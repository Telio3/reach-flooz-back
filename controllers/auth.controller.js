const { getUserByMail } = require('../queries/users.queries');

exports.signin = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        const user = await getUserByMail(email);
        if (user) {
            const match = await user.comparePassword(password);
            if (match) {
                res.cookie('idUser', user._id.toString());
                req.login(user);
                res.json(user);
            } else {
                res.status(400).send({ message: 'Email ou Mot de Passe incrorrect' });
            }
        } else {
            res.status(400).send({ message: 'Email ou Mot de Passe incrorrect' });
        }
    } catch (e) {
        next(e);
    }
};

exports.signout = (req, res, next) => {
    req.logout();
    res.status(200).send({ message: 'Vous a bien été déconnecté' });
};
