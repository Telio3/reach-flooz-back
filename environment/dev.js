const path = require('path');

module.exports = {
    cert: path.join(__dirname, '../ssl/server.cert'),
    key: path.join(__dirname, '../ssl/server.key'),
};
