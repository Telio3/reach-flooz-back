const jwt = require('jsonwebtoken');
const config = require('./config.conf');
const secret = config.SECRET;
const { app } = require('../app');
const { getUserById } = require('../queries/users.queries');

const createJwtToken = (user) => {
    const jwtToken = jwt.sign(
        {
            sub: user._id.toString(),
            exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 3,
        },
        secret
    );
    return jwtToken;
};

exports.createJwtToken = createJwtToken;

const extractUserFromToken = async (req, res, next) => {
    const token = req.cookies.jwt;
    if (token) {
        try {
            const decodedToken = jwt.verify(token, secret);
            const user = await getUserById(decodedToken.sub);
            if (user) {
                req.user = user;
                next();
            } else {
                res.clearCookie('jwt');
                res.clearCookie('idUser');
                res.end();
            }
        } catch (e) {
            res.clearCookie('jwt');
            res.clearCookie('idUser');
            res.end();
        }
    } else {
        next();
    }
};

const addJwtFeatures = (req, res, next) => {
    req.isAuthenticated = () => !!req.user;
    req.logout = () => {
        res.clearCookie('jwt');
        res.clearCookie('idUser');
    };
    req.login = (user) => {
        const token = createJwtToken(user);
        res.cookie('jwt', token);
    };
    next();
};

app.use(extractUserFromToken);
app.use(addJwtFeatures);
