exports.ensureAuthenticated = (req, res, next) => {
    if (req.isAuthenticated() && req.user._id == req.params.idUser) {
        next();
    } else {
        res.status(403).send({ message: 'Token non valide' });
    }
};
